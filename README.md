# stressTestSystemDocker

#### 介绍
压测平台系统stressTestSystem的Docker部署方案；本方案对应的平台源码 https://gitee.com/smooth00/stressTestSystem

#### mysql版部署方案
1、以CentOS7为例，安装docker环境
```
yum update -y
yum install docker epel-release python-pip -y
pip install --upgrade pip
pip install docker-compose
```
2、将编译好的renren-fast.jar放到 [mysql版/renren-fast/] 目录下<br/>
  如果想通过git拉取源码到镜像中编译，就需要修改renren-fast/Dockerfile，将java编译过程写入

3、将[mysql版]目录整体上传到/home目录下（可以指定别的目录）<br/>
同时要求创建mysql卷目录
```
mkdir -p /opt/mysql
chmod -R 777 /opt/mysql
```
4、环境变量配置文件 .env <br/>
默认不需要修改，如果需要分开不同主机部署，就要创建多个HOST_IP变量，同时需要修改docker-compose

5、整体构建数据库及平台镜像<br/>
docker-compose build <br/>

或直接构建并整体启动容器<br/>
docker-compose up 或者用 nohup docker-compose up &
<br/>如果要离线部署，就在连网的机器上先docker-compose build后将镜像打成tar包到目标机上解压，然后再用docker-compose up启动；
<br/>由于 docker-compose.yml 文件中配置映射出去的端口是8088（不是默认的8080），所以用 http 8088 访问压测平台（注意避免防火墙拦截）
<br/>
6、如果要批量清除容器和镜像
```
docker ps | grep renren- | grep -v "grep" | awk '{print $1}' | xargs docker rm -f
docker ps | grep stress- | grep -v "grep" | awk '{print $1}' | xargs docker rm -f
docker images | grep renren- | grep -v "grep" | awk '{print $3}' | xargs docker rmi -f
docker images | grep stress- | grep -v "grep" | awk '{print $3}' | xargs docker rmi -f
```
#### h2版部署方案

1、以CentOS7为例，安装docker环境
```
yum update -y
yum install docker epel-release -y
```
2、将编译好的renren-fast.jar放到当前[h2版]目录下<br/>
如果想通过git拉取源码到镜像中编译，就需要修改Dockerfile，将java编译过程写入

3、将[h2版]目录整体上传到/home目录下（可以指定别的目录）

4、进入[h2版]目录下构建平台镜像
```
docker build -t renren_fast_test:1.0 .
```
5、启动容器
```
docker run --name renren_fast_test -d --restart=unless-stopped -p 8088:8080 renren_fast_test:1.0
```
由于映射出去的端口是8088（不是默认的8080），所以用 http 8088 访问压测平台<br />
6、如果要清除容器和镜像
```
docker ps | grep renren_ | grep -v "grep" | awk '{print $1}' | xargs docker rm -f
docker images | grep renren_ | grep -v "grep" | awk '{print $3}' | xargs docker rmi -f
```
#### 分布式节点部署方案
1、可以采用docker-compose ，参考mysql版部署方案<br />
2、jmeter分布式节点部署要求：<br />
由于jmeter要求主从节点之间保持互通，要求端口要能够互访，所以Jmeter节点有必要采用docker版（Jmeter 5.1.1）<br />
jmeter-slave 的Dockerfile文件参考如下（我们压测平台和主从节点统一用的是5.1.1版本）：
```
# Use JDK-8 on alpine.
From openjdk:8-alpine
MAINTAINER smooth00 <smooth.blog.csdn.net>

ENV JMETER_VERSION 5.1.1

# Install Pre-requisite Packages like wget
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories && \
    apk update && apk add wget unzip vim && apk add -U tzdata && \
    ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone

# Installing jmeter
RUN mkdir /jmeter \
	&& cd /jmeter/ \
	&& wget https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-$JMETER_VERSION.tgz \
	&& tar -xzf apache-jmeter-$JMETER_VERSION.tgz \ 
	&& rm apache-jmeter-$JMETER_VERSION.tgz \
	&& mkdir -p /jmeter/apache-jmeter-$JMETER_VERSION/bin/stressTestCases \
	&& wget -P /jmeter/apache-jmeter-$JMETER_VERSION/lib/ext https://repo.maven.apache.org/maven2/kg/apc/jmeter-plugins-extras-libs/1.4.0/jmeter-plugins-extras-libs-1.4.0.jar \
	&& wget -P /jmeter/apache-jmeter-$JMETER_VERSION/lib/ext https://repo.maven.apache.org/maven2/kg/apc/jmeter-plugins-standard/1.4.0/jmeter-plugins-standard-1.4.0.jar

# Settingt Jmeter Home
ENV JMETER_HOME /jmeter/apache-jmeter-$JMETER_VERSION/

# Finally Adding Jmeter to the Path
ENV PATH $JMETER_HOME/bin:$PATH

# Volume directory to be mapped (for test files)
VOLUME $JMETER_HOME/bin/stressTestCases

# Ports to be exposed from the container for JMeter Slaves/Server
EXPOSE 1099 50000
ENV SSL_DISABLED true

# Application to run on starting the container
WORKDIR $JMETER_HOME/bin/stressTestCases
ENTRYPOINT ../jmeter-server \
                        -Dserver.rmi.localport=50000 \
                        -Dserver_port=1099 \
                        -Jserver.rmi.ssl.disable=${SSL_DISABLED} \
                        -Djava.rmi.server.hostname=${HOST_IP}
```
Dockerfile编译：docker build -t jmeter-slave:5.1.1 .<br />
注意：各个节点的时间同步非常重要，时间误差将会导致测试数据采样不准确<br />
jmeter会自动从官网下载，如果觉得下载慢，可以先下载然后通过COPY载入镜像。<br />
或者直接pull(镜像我已上传)：docker pull smooth00/jmeter-slave <br />
jmeter-slave容器启动：
```
docker run --name jmeter-slave -d --restart=unless-stopped -p 2099:1099 -p 50000:50000 -e HOST_IP=172.16.1.140 jmeter-slave:5.1.1
```
如果要同步参数化文件到Docker中，需要创建卷目录（建议分布式压测不要用参数化文件，效率不高）：
```
mkdir -p /opt/docker-jmeter/bin/stressTestCases
chmod 755 -R /opt/docker-jmeter/bin/stressTestCases
docker run --name jmeter-slave -d --restart=unless-stopped -p 2099:1099 -p 50000:50000 -v /opt/docker-jmeter/bin/stressTestCases:/jmeter/apache-jmeter-5.1.1/bin/stressTestCases -e HOST_IP=172.16.1.140 jmeter-slave:5.1.1
```
如果使用卷目录映射，那么平台界面上的分布节点管理，就需要正确的配置宿主机的jmeter_home路径(以便同步参数化文件)：/opt/docker-jmeter <br />
最最关键的一点，在docker中调用参数化文件和虚拟机中是一样的，脚本中引用参数一定不要设置路径（这样就能默认调用 $JMETER_HOME/bin/stressTestCases）， <br />
3、以上是jmeter-slave的Dockerfile，在build成镜像后可以通过docker-compose调用，Jmeter-slave和压测平台的集群部署docker-compose.yml样例参考如下：
```
version: '2'
services:
  jmeter-slave:
    image: jmeter-slave:5.1.1
    environment:
      HOST_IP: 172.16.1.140
    stdin_open: true
    network_mode: bridge
    tty: true
    ports:
    - 2099:1099/tcp
    - 50000:50000/tcp
  renren-fast:
    image: renren_fast_test:1.0
    stdin_open: true
    network_mode: bridge
    tty: true
    ports:
    - 8088:8080/tcp
```
**说明：**
由于jmeter-slave docker本身以服务方式自动启动，所以在压测平台里就只需要添加节点，并选择手工启动（SSH远程启动是不行的）.
<br />另外以上样例docker用的jmeter-slave端口是2099（创建多个docker实例记住端口不要冲突）。
