#!/bin/bash
set -e
 
#查看mysql服务的状态，方便调试，这条语句可以删除
echo `service mysql status`
 
echo '1.启动mysql....'
#启动mysql
service mysql start
sleep 3
echo `service mysql status`
 
echo '2.开始导入数据....'
#导入数据
if [ -d /var/lib/mysql/renren_fast ] ; then
  echo '3.数据库已存在，中止导入....'
else
  mysql < /mysql/db.sql
  echo '3.导入数据完毕....'
  sleep 3
  echo `service mysql status`
 
  #重新设置mysql权限
  echo '4.开始修改权限....'
  mysql < /mysql/privileges.sql
  echo '5.修改权限完毕....'
fi
#sleep 3
echo `service mysql status`
echo `mysql容器启动完毕!`
 
tail -f /dev/null
