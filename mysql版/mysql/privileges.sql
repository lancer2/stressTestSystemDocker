use mysql;
select host, user from user;
-- 因为mysql版本是5.7，因此授权用户为如下命令：
-- 正式环境注意别开启root的远程权限
-- grant all privileges on *.* to root@'%' identified by "123456";
create user 'test'@'%' identified by '123456';
grant all privileges on renren_fast.* to 'test'@'%' identified by '123456';
-- 这一条命令一定要有：
flush privileges;
select host, user from user;